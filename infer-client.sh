#!/bin/bash

set -e

SCRIPTDIR="$(dirname "$0")"
cd "$SCRIPTDIR"

CLIENT_IMAGE=$1
INFERER_IMAGE=tls-test-bed/pylstar-tls
VERSION=$2
VOCABULARY=$3

shift 3

PID=$$

if [ -z "$( docker network ls -f name=tls-test-bed -q )" ]; then
    docker network create tls-test-bed
fi

docker run -d --rm \
           --name "tls-client-$PID" \
           --network tls-test-bed \
           -v "$PWD"/crypto/material/trusted-ca/ca.pem:/tmp/ca.pem \
           "$CLIENT_IMAGE" /run_client.sh \
                           -p 4444 \
                           -V "$VERSION" \
                           $CIPHERS \
                           -A /tmp/ca.pem

[ -n "$RESULTS_DIR" ] || RESULTS_DIR="$(mktemp -d /tmp/tls-test-bed--XXXXXX)"
mkdir -p "$RESULTS_DIR"

make -C crypto certs/"tls-inferer-$PID".crt

docker run -i --rm \
           --name tls-inferer-$PID \
           -v "$RESULTS_DIR":/results \
           -v "$PWD"/crypto/material:/tmp/material \
           -v "$PWD"/crypto/certs:/tmp/certs \
           --network tls-test-bed \
           "$INFERER_IMAGE" /probe_client.sh \
                            -T "tls-client-$PID":4444 \
                            -S "$VOCABULARY" \
                            -L "tls-inferer-$PID":4433 \
                            -C valid:/tmp/certs/"tls-inferer-$PID".crt:/tmp/certs/"tls-inferer-$PID".key:DEFAULT \
                            -C invalid:/tmp/material/servers/invalid/cert.pem:/tmp/material/servers/invalid/key.pem \
                            -C untrusted:/tmp/material/servers/untrusted/cert.pem:/tmp/material/servers/untrusted/key.pem \
                            -o /results \
                            "$@"

docker kill "tls-client-$PID"

echo Results are in "$RESULTS_DIR"
