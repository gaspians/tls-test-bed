# TLS Test Bed

Collection of TLS Stack Containers and TLS Tools.

## Installation

You need to install Docker, make and openssl.


## Nano-tutorial

To infer an OpenSSL server, here are the steps required:

  * Generate some cryptographic material using `make crypto-material`
  * Find the tag you are interested in in the directories `stacks/openssl*/tags`; we will use `openssl-3.0.2`.
  * build the OpenSSL container using `make containers/openssl/openssl-3.0.2.docker`, which will produce the container named `tls-test-bed/openssl:openssl-3.0.2`, and put the image identifier in the file `containers/openssl/openssl-3.0.2.docker`.
  * build the `pylstar-tls` container, with `make containers/tools/pylstar-tls.docker`, which will produce the container named `tls-test-bed/pylstar-tls`.
  * run the inference using `./infer-server.sh tls-test-bed/openssl:openssl-3.0.2 1.3 tls13`, where `1.3` is the version used by the server and `tls13` corresponds to the scenario used by `pylstar-tls`.

The results will be in a directory called `/tmp/tls-test-bed-XXXXXX` but you can change that if you want, using the `RESULTS_DIR` environment variable.


Inferring a client can be done in a similar way.


You can run `tlsprint` on the different stacks using the following commands:

    make containers/tools/tlsprint.docker
    make containers/openssl/OpenSSL_1_1_1k.docker
    /tlsprint.sh tls-test-bed/openssl:OpenSSL_1_1_1k 1.2


## Bugs and enhancements

Please, report any bug found by opening a ticket and/or by submiting a pull requests.


## Authors and acknowledgment

  * Aina Toky Rasoamanana
  * Olivier Levillain

This tool has been supported in part by the French ANR [GASP project](https://gasp.ebfe.fr) (ANR-19-CE39-0001).


## License

This software is licensed under the GPLv3 License. See the `COPYING.txt` file
in the top distribution directory for the full license text.
